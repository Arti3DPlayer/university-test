# -*- coding: utf-8 -*-
from django.db import connection
from django.template import Template, Context

class QueriesLogMiddleware:
	def process_response ( self, request, response ):
		count = len(connection.queries)
		time = sum([float(q['time']) for q in connection.queries])

		response.content = response.content.replace('</body>', ('<meta content="text/html; Queries: %s, Count: %s"></body>')% (time,count))

		return response