# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import User
from django.utils.translation import ugettext_lazy as _

class UserAdmin(admin.ModelAdmin):
	fieldsets = ((None, {'fields': ('email', 'password')}),(_('Personal info'), {'fields': ('first_name','last_name')}),(_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser','groups', 'user_permissions')}),(_('Important dates'), {'fields': ('last_login', 'date_joined')}),)

	add_fieldsets = ((None, {'classes': ('wide',),'fields': ('email', 'password1', 'password2')}),)
    

admin.site.register(User, UserAdmin)