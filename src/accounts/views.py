# -*- coding: utf-8 -*-
from django.shortcuts import redirect, get_object_or_404
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.db.models import Count, Sum, Q

from .forms import AuthenticationForm, RegistrationForm
from .models import User

class SignIn(FormView):
	form_class = AuthenticationForm
	template_name = 'accounts/signin.html'
	success_url = '/groups/list/'

	def form_valid(self, form):
		auth.login(self.request, form._user)
		return super(SignIn, self).form_valid(form)

class SignUp(FormView):
	form_class = RegistrationForm
	template_name = 'accounts/signup.html'
	success_url = '/groups/list/'

	def form_valid(self, form):
		cd = form.cleaned_data
		User.objects.create_superuser(email= cd['email'], password=cd['password1'])
		user = auth.authenticate(email= cd['email'], password=cd['password1'])
		auth.login(self.request, user)
		return super(SignUp, self).form_valid(form)


class LogOut(View):
	def get(self, request, *args, **kwargs):
		if request.user.is_authenticated(): 
			auth.logout(request)
		return redirect('accounts:signin')