# -*- coding: utf-8 -*-
from django import forms
from django.contrib import auth
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from .models import User

class AuthenticationForm(forms.Form):
    email = forms.CharField(label=_(u'Email'), min_length=3, max_length=30)  
    password = forms.CharField(label=_(u'Пароль'), widget=forms.PasswordInput, min_length=6, max_length=30)

    def clean_email(self):
        email = self.cleaned_data['email']
        is_user = User.objects.filter(email=email).count()
        if not is_user: 
            raise forms.ValidationError(_(u'Такой e-mail адресс не зарегистрирован!')) 
        return email

    def clean_password(self):
        cd = self.cleaned_data
        if not self.errors:
            self._user = auth.authenticate(email=cd['email'], password=cd['password'])
            if self._user is None:
                raise forms.ValidationError(_(u'Неверный пароль!'))
        return cd["password"]


class RegistrationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        del self.fields['username']

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2')

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            User._default_manager.get(email=email)
            raise forms.ValidationError(_(u'Этот электронный адресс уже используется.'))
        except User.DoesNotExist:
            pass
        return email
        
        