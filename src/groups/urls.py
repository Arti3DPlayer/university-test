# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from .views import GroupList, GroupDetail

urlpatterns = patterns('',
	url(r'list/$', GroupList.as_view(),name='groups_list'),
	url(r'detail/([\w-]+)/$', GroupDetail.as_view(),name='groups_detail'),
)