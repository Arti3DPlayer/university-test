# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Group(models.Model):
	title = models.CharField(max_length=30, verbose_name=_(u'Название'))
	warden = models.ForeignKey('Student', blank=True, null=True, verbose_name=_(u'Староста'), related_name='student')

	class Meta:
		verbose_name = u'Группа'
		verbose_name_plural = u'Группы'
        ordering = ['title']

	def __unicode__(self):
		return self.title

class Student(models.Model):
    first_name = models.CharField(max_length=30, verbose_name=_(u'Фамилия'))
    middle_name = models.CharField(max_length=30, verbose_name=_(u'Имя'))
    last_name = models.CharField(max_length=30, verbose_name=_(u'Отчество'))
    birth_date = models.DateField(verbose_name=_(u'Дата рождения'))
    stud_number = models.CharField(max_length=30, verbose_name=_(u'№ Студ. билета'))
    group = models.ForeignKey('Group', verbose_name=_(u'Группа'))

    class Meta:
    	verbose_name = u'Студент'
    	verbose_name_plural = u'Студенты'
        ordering = ['first_name']

    def __unicode__(self):
        return "%s %s %s" % (self.first_name,self.middle_name,self.last_name)

