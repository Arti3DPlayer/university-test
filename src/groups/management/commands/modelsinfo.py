from django.core.management.base import AppCommand
from optparse import make_option

class Command( AppCommand ):
    help = 'Prints students and groups'

    def handle(self, *args, **options):
        from src.groups.models import Group, Student
        lines = []

        for item in Group.objects.all():
            lines.append(item.__unicode__())
        for item in Student.objects.all():
            lines.append(item.__unicode__())

        return "\n".join( lines )