# -*- coding: utf-8 -*-
from django.test import TestCase
from datetime import datetime
from django.test.client import Client

from .models import Student, Group
from src.accounts.models import User

class GroupTestCase(TestCase):
	def setUp(self):
		self.c = Client()
		self.user = User.objects.create_superuser(email='test@test.com',password='123456')
		self.group = Group.objects.create(title="404-OK")
		self.student = Student.objects.create(first_name="Петренко", middle_name="Пётр", last_name="Батькович", birth_date=datetime.today(), stud_number="33434", group=self.group)

	def test_signin(self):
		response = self.c.post('/accounts/signin/', {'email': 'test@test.com', 'password': '123456'})
		self.assertEqual(response.status_code, 302)

	def test_signup(self):
		response = self.c.post('/accounts/signup/', {'email': 'test1@test1.com', 'password1': '123456','password2': '123456'})
		self.assertEqual(response.status_code, 302)
