# -*- coding: utf-8 -*-
from django.conf import settings
from django import template
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, F, Avg, Count
from django.core.urlresolvers import reverse, resolve

register = template.Library()

@register.simple_tag()
def edit_object_url(obj):
	return "/admin/%s/%s/%s/" % (obj._meta.app_label, obj._meta.object_name.lower(),obj.id)