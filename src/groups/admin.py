# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Group, Student

class StudentInline(admin.TabularInline):
    model = Student

class GroupAdmin(admin.ModelAdmin):
    list_display = ('title',)
    inlines = [StudentInline,]

class StudentAdmin(admin.ModelAdmin):
    list_display = ('first_name','middle_name','last_name','group')

admin.site.register(Group,GroupAdmin)
admin.site.register(Student,StudentAdmin)

