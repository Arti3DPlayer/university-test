from django.shortcuts import redirect, get_object_or_404
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic import ListView
from django.db.models import Count, Sum, Q

from .models import Group, Student

class GroupList(ListView):
	model=Group
	template_name="groups/groups_list.html"
	context_object_name = 'groups'

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(GroupList, self).dispatch(*args, **kwargs)

class GroupDetail(ListView):
	template_name="groups/group_detail.html"
	context_object_name = 'students'

	def get_queryset(self):
		self.group = get_object_or_404(Group, id=self.args[0])
		return Student.objects.filter(group=self.group)

	def get_context_data(self, **kwargs):
		context = super(GroupDetail, self).get_context_data(**kwargs)
		context['group'] = self.group
		return context

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(GroupDetail, self).dispatch(*args, **kwargs)