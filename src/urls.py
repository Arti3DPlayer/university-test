from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import RedirectView
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', RedirectView.as_view(url='groups/list')),
	url(r'^accounts/', include('src.accounts.urls', 'accounts')),
	url(r'^groups/', include('src.groups.urls', 'groups')),

    url(r'^admin/', include(admin.site.urls)),
)
